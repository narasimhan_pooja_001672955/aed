/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;


public class Product {
    private String name;
    private int price;
    private int modelNumber;

    public String getName() {
        return name;
    }

    public void setName(String prodName) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    
    
    @Override
    public String toString() {
        return name;
    }
    
}
