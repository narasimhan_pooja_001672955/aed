/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Pooja Narasimhan
 */
public class PatientCatalog {
    private ArrayList<Patient> patientCatalog;
    public PatientCatalog(){
        patientCatalog = new ArrayList<Patient>();
    }

    public ArrayList<Patient> getPatientCatalog() {
        return patientCatalog;
    }

    public void setPatientCatalog(ArrayList<Patient> patientCatalog) {
        this.patientCatalog = patientCatalog;
    }
    
    public Patient createPatient(){
        Patient patient = new Patient();
        patientCatalog.add(patient);
        return  patient;
    }
    
    public int getAllocatedSize() {
        return patientCatalog.size();
    }
    
    public void deletePatient(Patient patient){
       
        patientCatalog.remove(patient);
    }
    
   
    
    
}
