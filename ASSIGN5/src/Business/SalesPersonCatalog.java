/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Pooja Narasimhan
 */
public class SalesPersonCatalog {
    
    ArrayList<SalesPersons> salesPersonCatalog;
    
    public SalesPersonCatalog(){
        salesPersonCatalog = new ArrayList<>();
    }

    public ArrayList<SalesPersons> getSalesPersonCatalog() {
        return salesPersonCatalog;
    }

    public void setSalesPersonCatalog(ArrayList<SalesPersons> salesPersonCatalog) {
        this.salesPersonCatalog = salesPersonCatalog;
    }
   
    public SalesPersons addSalesPerson(){
        SalesPersons sp = new SalesPersons();
        salesPersonCatalog.add(sp);
        return sp;
    }
    
}
