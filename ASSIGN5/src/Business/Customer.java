/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pooja Narasimhan
 */
public class Customer {
    String cusName;
    int Totalamount;

    public int getTotalamount() {
        return Totalamount;
    }

    public void setTotalamount(int Totalamount) {
        this.Totalamount = Totalamount;
    }
    MasterOrdercatalog masterOrdercatalog;
    public Customer(){
        masterOrdercatalog = new MasterOrdercatalog();
    }

    public MasterOrdercatalog getMasterOrdercatalog() {
        return masterOrdercatalog;
    }

    public void setMasterOrdercatalog(MasterOrdercatalog masterOrdercatalog) {
        this.masterOrdercatalog = masterOrdercatalog;
    }
    

    public String getCusName() {
        return cusName;
    }

    @Override
    public String toString() {
        return cusName; //To change body of generated methods, choose Tools | Templates.
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
    
    
}
