/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pooja Narasimhan
 */
public class Supplier {
    
    String supName;
    ProductCatalog productCatalog;
    
    public Supplier(){
        productCatalog = new ProductCatalog();
    }

    public String getSupName() {
        return supName;
    }

    public void setSupName(String supName) {
        this.supName = supName;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    @Override
    public String toString() {
        return supName; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
