/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Pooja Narasimhan
 */
public class CustomerCatalog {
    
    ArrayList<Customer> custoemerCatalog;
    
    public CustomerCatalog(){
        custoemerCatalog =  new ArrayList<>();
    }

    public ArrayList<Customer> getCustoemerCatalog() {
        return custoemerCatalog;
    }

    public void setCustoemerCatalog(ArrayList<Customer> custoemerCatalog) {
        this.custoemerCatalog = custoemerCatalog;
    }
    
    public Customer addCustomer(){
        Customer cus = new Customer();
        custoemerCatalog.add(cus);
        return cus;
    }
    
}
