/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pooja Narasimhan
 */
public class Product {
    
    String prodName;
    int prodID;
    int modelNumber;
    int avail;
    int avail1;
    int avail2;
    int floorPrice;
    int cielPrice;

    public int getAvail1() {
        return avail1;
    }

    public void setAvail1(int avail1) {
        this.avail1 = avail1;
    }

    public int getAvail2() {
        return avail2;
    }

    public void setAvail2(int avail2) {
        this.avail2 = avail2;
    }
    int targetPrice;

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }
    

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getProdID() {
        return prodID;
    }

    public void setProdID(int prodID) {
        this.prodID = prodID;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public int getAvail() {
        return avail;
    }

    public void setAvail(int avail) {
        this.avail = avail;
    }

    public int getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(int floorPrice) {
        this.floorPrice = floorPrice;
    }

    public int getCielPrice() {
        return cielPrice;
    }

    public void setCielPrice(int cielPrice) {
        this.cielPrice = cielPrice;
    }
    

    @Override
    public String toString() {
        return prodName; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
