/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Pooja Narasimhan
 */
public class MasterOrdercatalog {
    
    ArrayList<Order> masterOrder;
    public MasterOrdercatalog(){
        masterOrder = new ArrayList<>();
    }

    public ArrayList<Order> getMasterOrder() {
        return masterOrder;
    }

    public void setMasterOrder(ArrayList<Order> masterOrder) {
        this.masterOrder = masterOrder;
    }
    
     public Order addOrder(Order o) {
        masterOrder.add(o);
        return o;
    }
    
}
