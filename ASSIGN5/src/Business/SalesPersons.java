/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pooja Narasimhan
 */
public class SalesPersons {
    
    String salesPerson;
    int count=0;
    int belowTarg=0;
    int salesCommission =0;
    
    int aboveTargetPrice=0;
    MasterOrdercatalog masterOrdercatalog;

    public int getBelowTarg() {
        return belowTarg;
    }

    public void setBelowTarg(int belowTarg) {
        this.belowTarg = belowTarg;
    }

   

    public int getAboveTargetPrice() {
        return aboveTargetPrice;
    }

    public void setAboveTargetPrice(int aboveTargetPrice) {
        this.aboveTargetPrice = aboveTargetPrice;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    public SalesPersons(){
        masterOrdercatalog = new MasterOrdercatalog();
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public MasterOrdercatalog getMasterOrdercatalog() {
        return masterOrdercatalog;
    }

    public void setMasterOrdercatalog(MasterOrdercatalog masterOrdercatalog) {
        this.masterOrdercatalog = masterOrdercatalog;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public int getSalesCommission() {
        return salesCommission;
    }

    public void setSalesCommission(int salesCommission) {
        this.salesCommission = salesCommission;
    }

    
    @Override
    public String toString() {
        return salesPerson; //To change body of generated methods, choose Tools | Templates.
    }

   
    
    
}
