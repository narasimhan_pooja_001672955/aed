/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Pooja Narasimhan
 */
public class Order {
    
    ArrayList<OrderItem> order;
    int orderNumber;
    int commission;
    SalesPersons salesPersons;
    
    public Order(){
        order = new ArrayList<>();
    }

    public SalesPersons getSalesPersons() {
        return salesPersons;
    }

    public void setSalesPersons(SalesPersons salesPersons) {
        this.salesPersons = salesPersons;
    }

    public ArrayList<OrderItem> getOrder() {
        return order;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public void setOrder(ArrayList<OrderItem> order) {
        this.order = order;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
    public void removeOrderItem(OrderItem o) {
        order.remove(o);
    }
    
    public OrderItem addOrderItem(Product p, int q, int price, int amount) {
        OrderItem o = new OrderItem();
        o.setProduct(p);
        o.setQuantity(q);
        o.setSalesPrice(price);
        o.setAmount(amount);
        
        order.add(o);
        return o;
    }
    
    
    
    
            
    
}
